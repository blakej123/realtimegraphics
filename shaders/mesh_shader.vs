#version 460

uniform mat4 mvpMatrix;

layout (location=0) in vec3 vertexPos;
layout (location=1) in vec3 vertexTexCoord;
layout (location=6) in vec3 vertexNormal;

out vec3 texCoord;
out vec3 brightness;

vec3 calcLightContributionForPointLight(vec3 vertexPos, vec3 vertexNormal, vec3 L, vec3 Lcolour, vec3 LK){

		vec3 D = L - vertexPos;
		float lengthD = length(D);
		vec3 D_ = normalize(D);

		float a = 1.0/(LK.x + LK.y*lengthD + LK.z*lengthD*lengthD);

		float lambertian = clamp(dot(D_, vertexNormal), 0.0, 1.0);

		return lambertian * a * Lcolour;
}

vec3 Ldirect = vec3(-0.5, 1.0, -0.5); 
vec3 calcLightContributionForDirectionalLight(vec3 vertexPos, vec3 mNormal, vec3 Lcolour, vec3 Ldirect)
{

vec3 D_ = normalize(Ldirect);

float lambertian = clamp(dot(D_, mNormal), 0.0, 1.0);

return lambertian * Lcolour;
}


void main(void) {

	vec3 L[1];
	L[0] = vec3(-10.0, 0.0 , 0.0);

	vec3 Lcol[2];
	Lcol[0] = vec3(0.4, 0.4, 0.4);
	Lcol[1] = vec3(0.5, 0.4, 0.25);

	vec3 LK[1];
	LK[0] = vec3(1.0, 0.1, 0.0);

	vec3 LDirect[1];
	LDirect[0] = vec3(-1.0, 1.0, 1.0);

	brightness = vec3(0.5, 0.5, 0.5);

	brightness += calcLightContributionForPointLight(vertexPos, vertexNormal, L[0], Lcol[0], LK[0]);
	brightness += calcLightContributionForDirectionalLight(vertexPos, vertexNormal, Lcol[1], LDirect[0]);


	texCoord = vertexTexCoord;
	gl_Position = mvpMatrix * vec4(vertexPos,1.0);
}

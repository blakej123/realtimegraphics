// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <glew/glew.h>
#include <glew/wglew.h>
#include <GL\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "src/CGTexturedQuad.h"
#include "src/aiWrapper.h"
#include "src/CGPrincipleAxes.h"
#include "src/texture_loader.h"
#include "src/sceneVBOs.h"


using namespace std;
using namespace CoreStructures;

#pragma region Scene variables and resources

// Variables needed to track where the mouse pointer is so we can determine which direction it's moving in
int	mouse_x, mouse_y;
bool mDown = false;
bool wKeyDown, aKeyDown, sKeyDown, dKeyDown, qKeyDown, eKeyDown = false;

GUClock* mainClock = nullptr;

//
// Main scene resources
//
GUPivotCamera* mainCamera = nullptr;
CGPrincipleAxes* principleAxes = nullptr;
CGTexturedQuad* texturedQuad = nullptr;
const aiScene* aiBeast;
const aiScene* aiTank;
const aiScene* aiGround;
const aiScene* aiDeadTree;
const aiScene* aiRock;
const aiScene* aiShield;

GLuint beastTexture;
GLuint tankTexture;
GLuint groundTexture;
GLuint deadtreeTexture;
GLuint rockTexture;
GLuint shieldTexture;

sceneVBOs* Beast;
sceneVBOs* Tank;
sceneVBOs* Ground;
sceneVBOs* DeadTree;
sceneVBOs* Rock;
sceneVBOs* Shield;

GLuint meshShader;

float playerX = 1.0;
float playerY = 0.185;
float playerZ = 1.0;

#pragma endregion


#pragma region Function Prototypes

void init(int argc, char* argv[]); // Main scene initialisation function
void update(void); // Main scene update function
void display(void); // Main scene render function

// Event handling functions
void mouseButtonDown(int button_id, int state, int x, int y);
void mouseMove(int x, int y);
void mouseWheel(int wheel, int direction, int x, int y);
void keyDown(unsigned char key, int x, int y);
void closeWindow(void);
void reportContextVersion(void);
void reportExtensions(void);

#pragma endregion


int main(int argc, char* argv[])
{
	init(argc, argv);
	glutMainLoop();

	// Stop clock and report final timing data
	if (mainClock) {

		mainClock->stop();
		mainClock->reportTimingData();
		mainClock->release();
	}

	return 0;
}


void init(int argc, char* argv[]) {

	// Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE/* | GLUT_MULTISAMPLE*/);
	glutSetOption(GLUT_MULTISAMPLE, 4);

	// Setup window
	int windowWidth = 800;
	int windowHeight = 600;
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("3D Example 01");
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Register callback functions
	glutIdleFunc(update); // Main scene update function
	glutDisplayFunc(display); // Main render function
	glutKeyboardFunc(keyDown); // Key down handler
	glutMouseFunc(mouseButtonDown); // Mouse button handler
	glutMotionFunc(mouseMove); // Mouse move handler
	glutMouseWheelFunc(mouseWheel); // Mouse wheel event handler
	glutCloseFunc(closeWindow); // Main resource cleanup handler


	// Initialise glew
	glewInit();

	// Initialise OpenGL...

	wglSwapIntervalEXT(0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW); // Default anyway

	// Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Setup main camera
	float viewportAspect = (float)windowWidth / (float)windowHeight;
	mainCamera = new GUPivotCamera(1.0f, 1.0f, 5.0f, 35.0f, viewportAspect, 0.1f);

	//principleAxes = new CGPrincipleAxes();

	//texturedQuad = new CGTexturedQuad("..\\Common2\\Resources\\Textures\\bumblebee.png");

	/*aiBeast = aiImportModel(string("beast.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Beast = new sceneVBOs(aiBeast);
	beastTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\beast_texture.bmp", TextureProperties(false));*/

	aiTank = aiImportModel(string("Models\\NewTank.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Tank = new sceneVBOs(aiTank);
	tankTexture = fiLoadTexture("Textures\\diffuse.jpg", TextureProperties(false));

	aiGround = aiImportModel(string("Models\\Ground.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Ground = new sceneVBOs(aiGround);
	groundTexture = fiLoadTexture("Textures\\Ground.png", TextureProperties(false));

	aiDeadTree = aiImportModel(string("Models\\DeadTree.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	DeadTree = new sceneVBOs(aiDeadTree);
	deadtreeTexture = fiLoadTexture("Textures\\DeadTree.png", TextureProperties(false));

	aiRock = aiImportModel(string("Models\\Rock.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Rock = new sceneVBOs(aiRock);
	rockTexture = fiLoadTexture("Textures\\Rock.jpg", TextureProperties(false));

	aiShield = aiImportModel(string("Models\\Shield.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	Shield = new sceneVBOs(aiShield);
	shieldTexture = fiLoadTexture("Textures\\Shield.png", TextureProperties(false));

	meshShader = setupShaders(string("shaders\\mesh_shader.vs"), string("shaders\\mesh_shader.fs"));

	// Setup and start the main clock
	mainClock = new GUClock();
}

// Main scene update function (called by FreeGLUT's main event loop every frame) 
void update(void) {

	// Update clock
	mainClock->tick();

	// Redraw the screen
	display();

	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "CIS5013. Average fps: %.0f; Average spf: %f", mainClock->averageFPS(), mainClock->averageSPF() / 1000.0f);
	glutSetWindowTitle(timingString);

	if (wKeyDown == true)
	{
		playerX = 0.1f + playerX;
		wKeyDown = false;
	}
	if (aKeyDown == true)
	{
		playerZ = -0.1f + playerZ;
		aKeyDown = false;
	}
	if (sKeyDown == true)
	{
		playerX = -0.1f + playerX;
		sKeyDown = false;
	}
	if (dKeyDown == true)
	{
		playerZ = 0.1f + playerZ;
		dKeyDown = false;
	}
	if (qKeyDown == true)
	{
		playerY = -0.1f + playerY;
		qKeyDown = false;
	}
	if (eKeyDown == true)
	{
		playerY = 0.1f + playerY;
		eKeyDown = false;
	}
}


void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set viewport to the client area of the current window
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	// Get view-projection transform as a GUMatrix4
	GUMatrix4 T = mainCamera->projectionTransform() * mainCamera->viewTransform() * GUMatrix4::translationMatrix(-playerX, -playerY, -playerZ);

	if (principleAxes)
		principleAxes->render(T);

	if (texturedQuad)
		texturedQuad->render(T);

	// Render example model loaded from obj file
	/*glBindTexture(GL_TEXTURE_2D, beastTexture);
	glEnable(GL_TEXTURE_2D);
	aiRender(aiBeast, mainCamera);*/
	
	/*glBindTexture(GL_TEXTURE_2D, beastTexture);
	glEnable(GL_TEXTURE_2D);
	static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	Beast->render();*/
	

	glBindTexture(GL_TEXTURE_2D, tankTexture);
	glEnable(GL_TEXTURE_2D);
	static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");
	GUMatrix4 modelMatrix = GUMatrix4::translationMatrix(playerX, playerY, playerZ) * GUMatrix4::scaleMatrix(0.0001, 0.0001, 0.0001) * GUMatrix4::rotationMatrix(0.0, 1.57, 0.0);
	GUMatrix4 newT = T * modelMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(newT.M));
	Tank->render();

	glBindTexture(GL_TEXTURE_2D, groundTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 groundMatrix = GUMatrix4::translationMatrix(37.5, 6.0, 15.0) * GUMatrix4::scaleMatrix(0.003, 0.003, 0.003);
	GUMatrix4 groundT = T * groundMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(groundT.M));
	Ground->render();

	glBindTexture(GL_TEXTURE_2D, deadtreeTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 deadtreeMatrix = GUMatrix4::translationMatrix(7, 0.85, -3.25) * GUMatrix4::scaleMatrix(0.2, 0.2, 0.2);
	GUMatrix4 deadtreeT = T * deadtreeMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(deadtreeT.M));
	DeadTree->render();

	glBindTexture(GL_TEXTURE_2D, deadtreeTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 deadtree2Matrix = GUMatrix4::translationMatrix(14, 0.85, 3.25) * GUMatrix4::scaleMatrix(0.2, 0.2, 0.2);
	GUMatrix4 deadtree2T = T * deadtree2Matrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(deadtree2T.M));
	DeadTree->render();

	glBindTexture(GL_TEXTURE_2D, deadtreeTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 deadtree3Matrix = GUMatrix4::translationMatrix(18, 0.85, -3.25) * GUMatrix4::scaleMatrix(0.2, 0.2, 0.2);
	GUMatrix4 deadtree3T = T * deadtree3Matrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(deadtree3T.M));
	DeadTree->render();

	glBindTexture(GL_TEXTURE_2D, rockTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 rockMatrix = GUMatrix4::translationMatrix(20.0, 0.0, -3.0) * GUMatrix4::scaleMatrix(0.002, 0.002, 0.002);
	GUMatrix4 rockT = T * rockMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(rockT.M));
	Rock->render();

	glBindTexture(GL_TEXTURE_2D, rockTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 rock2Matrix = GUMatrix4::translationMatrix(20.0, 0.0, -1.0) * GUMatrix4::scaleMatrix(0.002, 0.002, 0.002);
	GUMatrix4 rock2T = T * rock2Matrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(rock2T.M));
	Rock->render();

	glBindTexture(GL_TEXTURE_2D, rockTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 rock3Matrix = GUMatrix4::translationMatrix(20.0, 0.0, 1.0) * GUMatrix4::scaleMatrix(0.002, 0.002, 0.002);
	GUMatrix4 rock3T = T * rock3Matrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(rock3T.M));
	Rock->render();

	glBindTexture(GL_TEXTURE_2D, rockTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 rock4Matrix = GUMatrix4::translationMatrix(20, 0.0, 3.0) * GUMatrix4::scaleMatrix(0.002, 0.002, 0.002);
	GUMatrix4 rock4T = T * rock4Matrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(rock4T.M));
	Rock->render();

	glBindTexture(GL_TEXTURE_2D, shieldTexture);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	GUMatrix4 shieldMatrix = GUMatrix4::translationMatrix(15.0, 0.0, 1.0) * GUMatrix4::scaleMatrix(0.2, 0.2, 0.2) * GUMatrix4::rotationMatrix(0.0, -1.57, 0.0);
	GUMatrix4 shieldT = T * shieldMatrix;
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(shieldT.M));
	Shield->render();
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	glutSwapBuffers();
}



#pragma region Event handling functions

void mouseButtonDown(int button_id, int state, int x, int y) {

	if (button_id == GLUT_LEFT_BUTTON) {

		if (state == GLUT_DOWN) {

			mouse_x = x;
			mouse_y = y;

			mDown = true;

		}
		else if (state == GLUT_UP) {

			mDown = false;
		}
	}
}


void mouseMove(int x, int y) {

	int dx = x - mouse_x;
	int dy = y - mouse_y;

	if (mainCamera)
		mainCamera->transformCamera((float)-dy, (float)-dx, 0.0f);

	mouse_x = x;
	mouse_y = y;
}


void mouseWheel(int wheel, int direction, int x, int y) {

	if (mainCamera) {

		if (direction < 0)
			mainCamera->scaleCameraRadius(1.1f);
		else if (direction > 0)
			mainCamera->scaleCameraRadius(0.9f);
	}
}


void keyDown(unsigned char key, int x, int y) {

	// Toggle fullscreen (This does not adjust the display mode however!)
	if (key == 'f')
		glutFullScreenToggle();
	if (key == 'w')
		wKeyDown = true;
	if (key == 'a')
		aKeyDown = true;
	if (key == 's')
		sKeyDown = true;
	if (key == 'd')
		dKeyDown = true;
	if (key == 'q')
		qKeyDown = true;
	if (key == 'e')
		eKeyDown = true;

}


void closeWindow(void) {

	// Clean-up scene resources

	if (mainCamera)
		mainCamera->release();

	if (principleAxes)
		principleAxes->release();

	if (texturedQuad)
		texturedQuad->release();

	//if (exampleModel)
		//exampleModel->release();
}


#pragma region Helper Functions

void reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char* glExtensionString = (const char*)glGetString(GL_EXTENSIONS);

	char* strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char* sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}

#pragma endregion


#pragma endregion


#include "meshVBOs.h"


meshVBOs::meshVBOs(const aiScene* scene, int meshIndex)
{

	// setup VAO for principle axes object
	glGenVertexArrays(1, &meshVAO);
	glBindVertexArray(meshVAO);
	 

	// setup vbo for position attribute
	glGenBuffers(1, &meshPosBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, meshPosBuffer);
	glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[meshIndex]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[meshIndex]->mVertices, GL_STATIC_DRAW);

	// setup vertex shader attribute bindings (connecting current <position> buffer to associated 'in' variable in vertex shader)
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

	//glGenVertexArrays(1, &lightVAO);
	//glBindVertexArray(lightVAO);


	// setup vbo for normal attribute
	glGenBuffers(1, &meshNormalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, meshNormalBuffer);
	glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[meshIndex]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[meshIndex]->mNormals, GL_STATIC_DRAW);

	// setup vertex shader attribute bindings (connecting current <position> buffer to associated 'in' variable in vertex shader)
	glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);


	// setup vbo for colour attribute
	glGenBuffers(1, &meshTexCoordBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, meshTexCoordBuffer);
	glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[meshIndex]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[meshIndex]->mTextureCoords[0], GL_STATIC_DRAW);

	// setup vertex shader attribute bindings (connecting current <colour> buffer to associated 'in' variable in vertex shader)
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, (const GLvoid*)0);

	// setup vbo for principle axis (pa) index buffer
	glGenBuffers(1, &meshFaceVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshFaceVBO);

	unsigned int numBytes = scene->mMeshes[meshIndex]->mNumFaces * 3 * sizeof(unsigned int);

	unsigned int *FaceIndexArray = (unsigned int*)malloc(numBytes);

	for (int f = 0, dstIndex = 0; f < scene->mMeshes[meshIndex]->mNumFaces; f++) {

		unsigned int* I = scene->mMeshes[meshIndex]->mFaces[f].mIndices;

		FaceIndexArray[dstIndex] = I[0];
		dstIndex += 1;
		FaceIndexArray[dstIndex] = I[1];
		dstIndex += 1;
		FaceIndexArray[dstIndex] = I[2];
		dstIndex += 1;

	}
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numBytes, FaceIndexArray, GL_STATIC_DRAW);

	// enable vertex buffers for principle axes rendering (vertex positions and colour buffers)
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(6);

	// unbind principle axes VAO
	glBindVertexArray(0);

	numfaces = scene->mMeshes[meshIndex]->mNumFaces;

}


void meshVBOs::render() {

	glBindVertexArray(meshVAO);
	glDrawElements(GL_TRIANGLES, numfaces * 3, GL_UNSIGNED_INT, (void*)0);
}

